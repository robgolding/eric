use Mix.Config

config :eric,
  port: System.get_env("PORT") || 4040
