defmodule Eric.Chat do
  use GenServer
  alias Eric.Client

  ## Client API

  def start_link(opts) do
    GenServer.start_link(__MODULE__, %{}, opts)
  end

  def join(pid, name, server) do
    GenServer.call(pid, {:join, {server, name}})
  end

  def leave(pid, server) do
    GenServer.call(pid, {:leave, server})
  end

  def ping(pid, server) do
    GenServer.call(pid, {:ping, server})
  end

  def rename(pid, server, new_name) do
    GenServer.call(pid, {:rename, server, new_name})
  end

  def who(pid, server) do
    GenServer.call(pid, {:who, server})
  end

  def broadcast(pid, message, server) do
    GenServer.call(pid, {:broadcast, message, server})
  end

  ## Server Callbacks

  def init(clients) do
    {:ok, clients}
  end

  def handle_call({:join, {client, name}}, _from, clients) do
    send_message("[sys]", "#{name} has joined", clients_excluding(clients, client))
    {:reply, :ok, Map.put(clients, client, name)}
  end

  def handle_call({:leave, client}, _from, clients) do
    name = Map.get(clients, client)
    send_message("[sys]", "#{name} has left!", clients_excluding(clients, client))
    {:reply, :ok, Map.delete(clients, client)}
  end

  def handle_call({:ping, client}, _from, clients) do
    send_message("[sys]", "pong!", [client])
    {:reply, :ok, clients}
  end

  def handle_call({:rename, client, new_name}, _from, clients) do
    old_name = Map.get(clients, client)
    send_message("[sys]", "#{old_name} is now known as #{new_name}", clients_excluding(clients, client))
    {:reply, :ok, %{ clients | client => new_name }}
  end

  def handle_call({:who, client}, _from, clients) do
    names = Map.values(clients)
    send_message("[sys]", "There are currently #{length(names)} users connected: #{Enum.join(names, ", ")}", [client])
    {:reply, :ok, clients}
  end

  def handle_call({:broadcast, message, client}, _from, clients) do
    from = Map.get(clients, client)
    send_message(from, message, clients_excluding(clients, client))
    {:reply, :ok, clients}
  end

  defp send_message(from, message, clients) do
    IO.inspect({from, message})
    Enum.each(clients, fn client -> Client.send(client, from, message) end)
    {:ok, clients}
  end

  defp clients_excluding(clients, client) do
    Enum.filter(Map.keys(clients), fn pid -> pid != client end)
  end
end
