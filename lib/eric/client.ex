defmodule Eric.Client do
  require Logger
  alias Eric.Chat
  use GenServer

  @initial_state %{state: :ident, name: nil, socket: nil}

  def start_link(socket, opts \\ []) do
    GenServer.start_link(__MODULE__, socket, opts)
  end

  def init(socket) do
    {:ok, _send("What's your name? ", %{ @initial_state | socket: socket })}
  end

  def send(pid, from, message) do
    GenServer.cast(pid, {:send, from, message})
  end

  ###

  def handle_cast({:send, from, message}, state) do
    {:noreply, _prompt(_send("\r#{from}: #{message}\n", state))}
  end

  def handle_info({:tcp, socket, data}, state) do
    state = process_message(String.trim(data), state)
    :inet.setopts(socket, [active: :once])
    {:noreply, _prompt(state)}
  end

  def handle_info({:tcp_closed, _socket}, %{state: :ident} = state) do
    _exit()
  end

  def handle_info({:tcp_closed, _socket}, state) do
    _close(state)
  end

  defp _prompt(%{socket: socket, name: name} = state) do
    :gen_tcp.send(socket, "\r#{name}: ")
    state
  end

  defp _close(_state) do
    Chat.leave(Chat, self())
    _exit()
  end

  defp _exit() do
    Process.exit(self(), :normal)
  end

  defp _send(message, %{socket: socket} = state) do
    :gen_tcp.send(socket, message)
    state
  end

  defp _ping(state) do
    Chat.ping(Eric.Chat, self())
    state
  end

  defp _rename(new_name, state) do
    Chat.rename(Eric.Chat, self(), new_name)
    %{ state | name: new_name }
  end

  defp _who(state) do
    Chat.who(Eric.Chat, self())
    state
  end

  defp _broadcast(message, state) do
    Chat.broadcast(Chat, message, self())
    state
  end

  defp process_message(name, %{state: :ident} = state) do
    Chat.join(Chat, name, self())
    %{ state | name: name, state: :run }
  end

  defp process_message(message, %{state: :run} = state) do
    case message do
      "/leave" ->
        _close(state)

      "/ping" ->
        _ping(state)

      "/name " <> new_name ->
        _rename(new_name, state)

      "/who" ->
        _who(state)

      _ ->
        _broadcast(message, state)
    end
  end
end
