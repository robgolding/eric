defmodule Eric do
  use Application

  defp get_port() do
    parse_port(Application.get_env(:eric, :port))
  end

  defp parse_port(port) when is_integer(port), do: port

  defp parse_port(port) when is_binary(port), do: String.to_integer(port)

  def start(_type, _args) do
    children = [
      {Eric.Chat, name: Eric.Chat},
      {Eric.Server, get_port},
      {DynamicSupervisor, strategy: :one_for_one, name: Eric.ClientSupervisor},
    ]
    Supervisor.start_link(children, strategy: :one_for_one)
  end
end
